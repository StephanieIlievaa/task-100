# A React Task
* In this task, we have to convert an already existing CSS file to **inline CSS**.
## Objective
* Checkout the dev branch.
*  We are provided with a functioning card component which is implemented in the App.js file.
* There are also styles in the App.css file. We must use those styles and convert them to **inline CSS**  whithin App.js. Each style must **not** be a separate object.
* Each element has its own **className** so you will be able to figure out which styles correspond to each element.
## Requirements
* The project starts with **npm run start**
* It's **mandatory** to remove all the declared classNames from the form and use only inline CSS.
* When implemented merge the dev branch to master.
## Gotchas
Read more about **CSS** - https://www.w3schools.com/css/css_howto.asp